#include "DerivativeCalculator.h"

double dneural::DerivativeCalculator::calculateDerivative(
	double value, ActivationFunctionType function)
{
	switch (function)
	{
	case ActivationFunctionType::sigmoid:
		return exp(value) / pow(1.0 + exp(value), 2.0);
		// Fun(?) fact: the correct formula of the sigmoid derivative should
		// have "-" before the values, but the result is the same either way

	case ActivationFunctionType::hyperbolicTangent:
		return pow(cosh(value), -2.0);

	case ActivationFunctionType::ReLU:
		return (value > 0.0) ? (1.0) : (0.0);

	case ActivationFunctionType::LReLU:
		return (value > 0.0) ? (1.0) : (0.01);

	case ActivationFunctionType::identity:
		return 1.0;

	case ActivationFunctionType::cube:
		return 3.0 * value * value;

	default:
		// Returns value if no matching function exists,
		// throw exception if is in debug configuration

#ifdef _DEBUG
		throw 42;
#else
		return value;
#endif
	}
}
