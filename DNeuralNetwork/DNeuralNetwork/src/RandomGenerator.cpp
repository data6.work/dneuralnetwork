#include "RandomGenerator.h"

namespace dneural
{

std::default_random_engine RandomGenerator::randomEngine =
	std::default_random_engine((unsigned)std::time(0));

} // namespace
