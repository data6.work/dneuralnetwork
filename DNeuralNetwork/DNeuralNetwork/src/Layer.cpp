#include "Layer.h"
#include "..\include\Layer.h"

namespace dneural
{

Layer::Layer(size_t nodeAmount)
{
	this->createNodes(nodeAmount);
}


Layer::Layer(
	size_t nodeAmount,
	ActivationFunctionType functionType) :
	activationFunctionType(functionType)
{
	this->createNodes(nodeAmount);
}


void Layer::connectToLayer(Layer* connectTo)
{
	// gets needed connection amount and creates it
	size_t connectionsNeeded =
		this->getNodeAmount() * connectTo->getNodeAmount();
	this->createConnections(connectionsNeeded);
}


size_t Layer::getNodeAmount()
{
	return this->nodes.size();
}


size_t Layer::getConnectionsAmount()
{
	return this->connections.size();
}


void Layer::forceLayerFire(
	const std::vector<double>& outputs,
	Layer* nextLayer)
{
	// copies input vector to node outputs
	size_t nodeAmount = this->getNodeAmount();
	for (size_t i = 0; i < nodeAmount; ++i)
	{
		this->nodes[i].setOutput(outputs[i]);
	}

	this->fireNodes(nextLayer);
}


std::vector<double> Layer::getOutputVector()
{
	std::vector<double> outputVector;
	for (Node& node : this->nodes)
	{
		outputVector.emplace_back(node.getOutput());
	}

	return outputVector;
}


void Layer::setWeights(const std::vector<double>& weights)
{
	size_t weightAmount = this->getConnectionsAmount();
	for (size_t index = 0; index < weightAmount; ++index)
	{
		this->connections[index].setWeight(weights[index]);
	}
}


void Layer::setBiases(const std::vector<double>& biases)
{
	size_t nodeAmount = this->getNodeAmount();
	for (size_t i = 0; i < nodeAmount; ++i)
	{
		this->nodes[i].setBias(biases[i]);
	}
}


double& Layer::getWeightByIndex(size_t index)
{
	return this->connections[index].getWeight();
}


double& Layer::getBiasByIndex(size_t index)
{
	return this->nodes[index].getBias();
}


std::vector<double> Layer::getWeights()
{
	std::vector<double> weights;
	weights.reserve(this->getConnectionsAmount());

	for (Connection& connection : this->connections)
	{
		weights.emplace_back(connection.getWeight());
	}

	return weights;
}


std::vector<double> Layer::getBiases()
{
	std::vector<double> biases;
	biases.reserve(this->getNodeAmount());

	for (Node& node : this->nodes)
	{
		biases.emplace_back(node.getBias());
	}

	return biases;
}


double Layer::getOutputByNodeIndex(size_t index)
{
	return this->nodes[index].getOutput();
}


void Layer::mutateRandomWeight(double mutationScale)
{
	size_t weightToMutate = RandomGenerator::generateRandomInteger<size_t>(
		0, this->getConnectionsAmount() - 1);

	this->connections[weightToMutate].setWeight(
		this->connections[weightToMutate].getWeight() + mutationScale);
}


void Layer::mutateRandomBias(double mutationScale)
{
	size_t biasToMutate = RandomGenerator::generateRandomInteger<size_t>(
		0, this->getNodeAmount() - 1);

	this->nodes[biasToMutate].setBias(
		this->nodes[biasToMutate].getBias() + mutationScale);
}


void Layer::randomizeWeights()
{
	for (Connection& connection : this->connections)
	{
		connection.randomizeWeight();
	}
}


void Layer::randomizeBiases()
{
	for (Node& node : this->nodes)
	{
		node.randomizeBias();
	}
}


void Layer::processLayer(Layer* nextLayer)
{
	// forces each node to calculate its output
	this->calculateNodeOutputs();
	this->fireNodes(nextLayer);
}


void Layer::calculateNodeOutputs()
{
	size_t nodeAmount = this->getNodeAmount();
	for (size_t i = 0; i < nodeAmount; ++i)
	{
		this->nodes[i].calculateOutput(this->getActivationFunction());
	}
}


bool Layer::operator==(const Layer& other)
{
	bool areSame =
		std::equal(
			this->nodes.begin(),
			this->nodes.end(),
			other.nodes.begin()) &&
		std::equal(
			this->connections.begin(),
			this->connections.end(),
			other.connections.begin()) &&
		(this->activationFunctionType == other.activationFunctionType);

	return areSame;
}


void Layer::createNodes(size_t nodeAmount)
{
	this->nodes.resize(nodeAmount);
	this->nodes.shrink_to_fit();
}


void Layer::createConnections(size_t connectionAmount)
{
	this->connections.resize(connectionAmount);
	this->connections.shrink_to_fit();
}


void Layer::setActivationFunction(ActivationFunctionType type)
{
	this->activationFunctionType = type;
}


ActivationFunctionType& Layer::getActivationFunction()
{
	return this->activationFunctionType;
}


void Layer::fireNodes(Layer* nextLayer)
{
	size_t nodeAmount = this->getNodeAmount();
	size_t nextLayerSize = nextLayer->getNodeAmount();

	// stores a pointer to the current layer's outgoing connections
	Connection* outgoingConnection = &this->connections.front();

	// itterates through each node of the next layer
	for (size_t nextLayerNode = 0;
		nextLayerNode < nextLayerSize;
		++nextLayerNode)
	{
		// resets next layer node input sum to 0
		nextLayer->nodes[nextLayerNode].setInputSum(0.0);

		// itterates through each node in this layer
		for (size_t node = 0; node < nodeAmount; ++node)
		{
			// sends current node's output to the next layer's node
			this->nodes[node].sendOutputTo(
				&nextLayer->nodes[nextLayerNode],
				outgoingConnection);

			// switches to the next outgoing connection
			outgoingConnection++;
		}
	}
}

} // namespace
