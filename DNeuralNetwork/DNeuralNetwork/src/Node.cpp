#include "Node.h"

namespace dneural
{

Node::Node()
{
	this->bias = 0.0;
}

Node::Node(double bias) :

	bias(bias)
{
}


double& Node::calculateOutput(ActivationFunctionType functionType)
{
	this->output = this->activationFunction(
		functionType,
		this->inputSum + this->bias);
	return this->output;
}


void Node::randomizeBias()
{
	this->bias =
		RandomGenerator::generateRandomDouble(-1.0, 1.0);
}


void Node::sendOutputTo(Node* target, Connection* connection)
{
	target->changeInputSum(
		connection->generateOutput(this->getOutput()));
}


void Node::setOutput(double newOutput)
{
	this->output = newOutput;
}


double& Node::getOutput()
{
	return this->output;
}


void Node::setBias(double newBias)
{
	this->bias = newBias;
}


void Node::changeBias(double changeBy)
{
	this->bias += changeBy;
}


double& Node::getBias()
{
	return this->bias;
}


void Node::setInputSum(double newSum)
{
	this->inputSum = newSum;
}


void Node::changeInputSum(double changeBy)
{
	this->inputSum += changeBy;
}


double& Node::getInputSum()
{
	return this->inputSum;
}


Node::operator double()
{
	return this->bias;
}


bool Node::operator==(const Node& other)
{
	return (this->bias == other.bias);
}


double Node::activationFunction(ActivationFunctionType type,
								double value)
{
	switch (type)
	{
	case ActivationFunctionType::sigmoid:
		return (1.0 / (1.0 + exp(-value)));

	case ActivationFunctionType::hyperbolicTangent:
		return tanh(value);

	case ActivationFunctionType::ReLU:
		return (value > 0.0) ? (value) : (0.0);

	case ActivationFunctionType::LReLU:
		return (value > 0.0) ? (value) : (0.01 * value);

	case ActivationFunctionType::identity:
		return value;

	case ActivationFunctionType::cube:
		return pow(value, 3.0);

	default:
		// TODO: do something about the incorrect values
		return 0;
	}
}

} // namespace
