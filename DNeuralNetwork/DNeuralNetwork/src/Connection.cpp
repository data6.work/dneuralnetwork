#include "Connection.h"

namespace dneural
{

Connection::Connection()
{
	this->randomizeWeight();
}


Connection::Connection(double startingWeight)
{
	this->weight = startingWeight;
}


double& Connection::getWeight()
{
	return this->weight;
}


void Connection::setWeight(double newWeight)
{
	this->weight = newWeight;
}


void Connection::changeWeight(double changeBy)
{
	this->weight += changeBy;
}


double Connection::generateOutput(double input)
{
	return input * this->weight;
}


void Connection::randomizeWeight()
{
	this->weight =
		RandomGenerator::generateRandomDouble(-2.0, 2.0);
}


Connection::operator double()
{
	return this->weight;
}


bool Connection::operator==(const Connection& other)
{
	return (this->weight == other.weight);
}

}
