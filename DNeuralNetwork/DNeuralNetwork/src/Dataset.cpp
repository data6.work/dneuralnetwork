#include "Dataset.h"

namespace dneural
{

const Dataset::Example& Dataset::operator[](size_t exampleIndex) const
{
	return this->examples[exampleIndex];
}


Dataset::Dataset(
	const std::vector<
		std::pair<
			std::vector<double>,
			std::vector<double>>>& trainingExampels)
{
	(*this) = trainingExampels;
}


Dataset& Dataset::operator=(
	const std::vector<
		std::pair<
			std::vector<double>,
			std::vector<double>>>& trainingExamples)
{
	examples.clear();
	examples.reserve(trainingExamples.size());

	// assigns each example
	for (auto& inputOutputPair : trainingExamples)
	{
		this->examples.emplace_back(Example(inputOutputPair));
	}

	return *this;
}


size_t Dataset::size() const
{
	return this->examples.size();
}


Dataset::Example::Example(
	const std::pair<
		std::vector<double>,
		std::vector<double>>& inputOutputPair)
{
	(*this) = inputOutputPair;
}


Dataset::Example& Dataset::Example::operator=(
	const std::pair<
		std::vector<double>,
		std::vector<double>>& inputOutputPair)
{
	this->inputs = inputOutputPair.first;
	this->outputs = inputOutputPair.second;

	return *this;
}

} // namespace
