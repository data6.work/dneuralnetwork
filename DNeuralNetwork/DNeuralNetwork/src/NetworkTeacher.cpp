#include "NetworkTeacher.h"

namespace dneural
{

void NetworkTeacher::teachUnilLearningLoss(
	DNeuralNetwork* network,
	const Dataset& dataset,
	double lossRequired,
	double learningRate)
{
	do
	{
		NetworkTeacher::trainOnDataset(
			network,
			dataset,
			learningRate);
	} while (NetworkTeacher::getLossOnDataset(network, dataset) > lossRequired);
}


void NetworkTeacher::teachForItterations(
	DNeuralNetwork* network,
	const Dataset& dataset,
	size_t itterationsAmount,
	double learningRate)
{
	for (size_t i = 0; i < itterationsAmount; ++i)
	{
		NetworkTeacher::trainOnDataset(
			network,
			dataset,
			learningRate);
	}
}


void NetworkTeacher::trainOnDataset(
	DNeuralNetwork* network,
	const Dataset& dataset,
	double learningRate)
{
	// stores copy of the weights and biases to apply changes to them
	std::vector<std::vector<double>> newWeights =
		network->getWeightsPerLayer();
	std::vector<std::vector<double>> newBiases =
		network->getBiasesPerLayer();

	size_t outputsAmount =
		network->getNodeAmountInLayer(network->getLayerAmount() - 1);
	size_t datasetSize = dataset.size();

	// creates error values storage for each node,
	// to pass them to backprop function
	std::vector<std::vector<double>> errors;
	errors.reserve(network->getLayerAmount());
	for (size_t layer = 0; layer < network->getLayerAmount(); ++layer)
	{
		// initializes inner vectors
		errors.emplace_back(std::vector<double>(
			network->getNodeAmountInLayer(layer), 0.0));
	}

	// Itterates through each inputs-outputs pairs in dataset
	for (size_t set = 0; set < datasetSize; ++set)
	{
		// processes and gets outputs
		network->generateOutputs(dataset[set].inputs);
		std::vector<double> networkOutputs = network->getOutputs();

		// saves last layer errors for each output
		nullify2DVector(&errors);
		for (size_t i = 0; i < outputsAmount; ++i)
		{
			errors.back()[i] =
				(networkOutputs[i] - dataset[set].outputs[i]) *
				learningRate;
		}

		// sets newWeights and newBiases to better fit the expected output
		NetworkTeacher::backpropagate(
			network,
			newWeights,
			newBiases,
			errors);
	}

	// applies the new weights
	network->copyWeightsFrom(newWeights);
	network->copyBiasesFrom(newBiases);
}


void NetworkTeacher::backpropagate(
	DNeuralNetwork* network,
	std::vector<std::vector<double>>& newWeights,
	std::vector<std::vector<double>>& newBiases,
	std::vector<std::vector<double>>& errors)
{
	size_t layerAmount = network->getLayerAmount();

	// saves the layer container reference for ease of access
	std::vector<Layer>& layers = network->layers;

	// backpropagate from the last layer
	for (size_t layer = layerAmount - 1; layer > 0; --layer)
	{
		std::vector<Node>& nodes = layers[layer].nodes;
		size_t nodeAmount = layers[layer].getNodeAmount();
		ActivationFunctionType function =
			layers[layer].getActivationFunction();

		Node* preceedingNodes = &layers[layer - 1].nodes.front();
		size_t preceedingNodeAmount = layers[layer - 1].getNodeAmount();
		Connection* preceedingConnections =
			&layers[layer - 1].connections.front();
		size_t preceedignConnectionIndex = 0;

		// itterates every node in the layer
		for (size_t node = 0; node < nodeAmount; ++node)
		{
			// stores output rate of change * error for later calcualtions
			double outputROConError =
				DerivativeCalculator::calculateDerivative(
					nodes[node].getInputSum() + nodes[node].getBias(),
					function) *
				errors[layer][node];

			// alters the bias
			newBiases[layer][node] += -outputROConError;

			// itterates through preceeding connections
			for (size_t i = 0;
				i < preceedingNodeAmount;
				++i, ++preceedignConnectionIndex)
			{
				// get new weight for preceeding conenction
				newWeights[layer - 1][preceedignConnectionIndex] +=
					-outputROConError * preceedingNodes[i].getOutput();

				// alter preceeding node's error value
				errors[layer - 1][i] +=
					outputROConError *
					preceedingConnections[preceedignConnectionIndex].getWeight();
			}
		}
	}
}


std::vector<std::vector<double>> NetworkTeacher::create2DVectorArray(
	std::vector<size_t> vectorSizes)
{
	std::vector<std::vector<double>> vector;

	vector.reserve(vectorSizes.size());
	for (size_t size : vectorSizes)
	{
		vector.emplace_back(std::vector<double>(size));
	}

	return vector;
}


void NetworkTeacher::nullify2DVector(std::vector<std::vector<double>>* vector)
{
	for (std::vector<double>& column : *vector)
	{
		for (double& value : column)
		{
			value = 0.0;
		}
	}
}


double NetworkTeacher::getLossOnDataset(
	DNeuralNetwork* network,
	const Dataset& dataset)
{
	double loss = 0.0;

	for (auto& example : dataset.examples)
	{
		network->generateOutputs(example.inputs);
		auto netOutputs = network->getOutputs();

		size_t outputSize = netOutputs.size();
		for (size_t i = 0; i < outputSize; ++i)
		{
			loss += pow(netOutputs[i] - example.outputs[i], 2.0);
		}
	}

	return loss / dataset.size();
}

} // namespace
