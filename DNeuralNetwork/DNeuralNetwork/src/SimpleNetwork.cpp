#include "SimpleNetwork.h"

namespace dneural
{

SimpleNetwork::SimpleNetwork(const std::vector<size_t>& nodesPerLayer) :
	network(nodesPerLayer)
{
}

std::vector<double> SimpleNetwork::process(const std::vector<double>& inputs)
{
	this->network.generateOutputs(inputs);
	return this->network.getOutputs();
}


void SimpleNetwork::teach(const Dataset& dataset, double learningRate)
{
	NetworkTeacher teacher;
	teacher.teachForItterations(
		&this->network,
		dataset,
		1,
		learningRate);
}


void SimpleNetwork::mutate(double mutationScale)
{
	this->network.mutate(mutationScale);
}


void SimpleNetwork::operator=(const SimpleNetwork& other)
{
	this->network = other.network;
}


void SimpleNetwork::operator=(const DNeuralNetwork& other)
{
	this->network = other;
}


SimpleNetwork::operator DNeuralNetwork*()
{
	return &this->network;
}


SimpleNetwork::operator DNeuralNetwork&()
{
	return this->network;
}

} // namespace
