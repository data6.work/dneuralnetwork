#include "NetworkFileIO.h"

namespace dneural
{

void NetworkFileIO::save(
	DNeuralNetwork* network,
	const std::string& filename)
{
	std::ofstream outputFile(filename, std::ios::binary | std::ios::out);

	NetworkFileIO::saveParameters(network, &outputFile);

	outputFile.close();
}


void NetworkFileIO::load(
	DNeuralNetwork* network,
	const std::string& filename)
{
	std::ifstream inputFile(filename, std::ios::binary | std::ios::in);

	network->clear();
	NetworkFileIO::loadParameters(network, &inputFile);

	inputFile.close();
}


DNeuralNetwork NetworkFileIO::load(const std::string& filename)
{
	DNeuralNetwork net;

	NetworkFileIO::load(&net, filename);
	return net;
}


void NetworkFileIO::saveParameters(
	DNeuralNetwork* network,
	std::ofstream* file)
{
	NetworkFileIO::saveStructuralParameters(network, file);
	NetworkFileIO::saveValueParameters(network, file);
}


void NetworkFileIO::saveStructuralParameters(
	DNeuralNetwork* network,
	std::ofstream* file)
{
	// total layer amount saving
	uint32_t layerAmount = (uint32_t)network->getLayerAmount();
	NetworkFileIO::writeValue(layerAmount, file);

	uint32_t maxLayerIndex = layerAmount - 1;

	// saves node amounts per layer
	for (uint32_t layer = 0; layer <= maxLayerIndex; ++layer)
	{
		uint32_t nodesPerlayer =
			(uint32_t)network->getNodeAmountInLayer(layer);
		NetworkFileIO::writeValue(nodesPerlayer, file);
	}

	// saves activation functions per layer
	for (uint32_t layer = 0; layer <= maxLayerIndex; ++layer)
	{
		uint32_t functionType = (ActivationFunctionType)
			network->getActivationFunctionInLayer(layer);
		NetworkFileIO::writeValue(functionType, file);
	}
}


void NetworkFileIO::saveValueParameters(
	DNeuralNetwork* network,
	std::ofstream* file)
{
	uint32_t maxLayerIndex = uint32_t(network->getLayerAmount() - 1);

	// saves biases
	for (uint32_t layer = 0; layer <= maxLayerIndex; ++layer)
	{
		uint32_t nodeAmount = uint32_t(network->getNodeAmountInLayer(layer));
		for (uint32_t i = 0; i < nodeAmount; ++i)
		{
			double bias = network->getBiasInLayer(layer, i);
			NetworkFileIO::writeValue(bias, file);
		}
	}

	// saves weights, unless this is the output layer
	for (uint32_t layer = 0; layer < maxLayerIndex; ++layer)
	{
		uint32_t weightsInLayer =
			(uint32_t)network->getConnectionAmountInLayer(layer);
		for (uint32_t i = 0; i < weightsInLayer; ++i)
		{
			double weight = network->getWeightInLayer(layer, i);
			NetworkFileIO::writeValue(weight, file);
		}
	}
}


void NetworkFileIO::loadParameters(
	DNeuralNetwork* network,
	std::ifstream* file)
{
	NetworkFileIO::loadStructuralParameters(network, file);
	NetworkFileIO::loadValueParameters(network, file);
}


void NetworkFileIO::loadStructuralParameters(
	DNeuralNetwork* network,
	std::ifstream* file)
{
	// stores extracted network structure
	std::vector<size_t> nodesPerLayer;

	// layer size loading
	uint32_t layerAmount;
	NetworkFileIO::readValue(
		&layerAmount,
		file);
	nodesPerLayer.resize(layerAmount);

	uint32_t maxlayerIndex = layerAmount - 1;

	// loads node amount in layers
	for (uint32_t layer = 0; layer <= maxlayerIndex; ++layer)
	{
		// loads node amount in layers
		uint32_t nodesInLayer;
		NetworkFileIO::readValue(
			&nodesInLayer,
			file);
		nodesPerLayer[layer] = nodesInLayer;
	}

	// initializes all layers
	network->createNetworkStructure(nodesPerLayer);

	// loads activation functions per layer
	for (uint32_t layer = 0; layer <= maxlayerIndex; ++layer)
	{
		uint32_t functionType;
		NetworkFileIO::readValue(&functionType, file);
		network->setActivationFunctionInLayer(
			layer,
			(ActivationFunctionType)functionType);
	}
}


void NetworkFileIO::loadValueParameters(
	DNeuralNetwork* network,
	std::ifstream* file)
{
	uint32_t maxLayerIndex = uint32_t(network->getLayerAmount() - 1);

	// node biases loading
	// NYI!
	// TODO: make this thing work propperly when biases are introduced
	for (uint32_t layer = 0; layer <= maxLayerIndex; ++layer)
	{
		uint32_t nodesInLayer =
			(uint32_t)network->getNodeAmountInLayer(layer);
		for (uint32_t i = 0; i < nodesInLayer; ++i)
		{
			double& bias = network->getBiasInLayer(layer, i);
			NetworkFileIO::readValue(&bias, file);
		}
	}

	// loads weights unless this is the output layer
	for (uint32_t layer = 0; layer < maxLayerIndex; ++layer)
	{
		uint32_t weightsAmount =
			(uint32_t)network->getConnectionAmountInLayer(layer);
		for (uint32_t i = 0; i < weightsAmount; ++i)
		{
			double& weight = network->getWeightInLayer(layer, i);
			NetworkFileIO::readValue(&weight, file);
		}
	}
}

} // namespace
