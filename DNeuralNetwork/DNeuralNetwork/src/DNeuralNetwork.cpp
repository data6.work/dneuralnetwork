#include "DNeuralNetwork.h"

namespace dneural
{


DNeuralNetwork::DNeuralNetwork(const std::vector<size_t>& nodesPerLayer)
{
	this->createNetworkStructure(nodesPerLayer);
}


DNeuralNetwork::DNeuralNetwork(
	const std::vector<std::pair<size_t, ActivationFunctionType>>&
	nodesAndFunctionPerLayer)
{
	this->createNetworkStructure(nodesAndFunctionPerLayer);
}


void DNeuralNetwork::createNetworkStructure(const std::vector<size_t>& nodesPerLayer)
{
	// clears any existing layers before creating them
	this->clear();

	// creates layers with specified node amounts
	size_t layerAmount = nodesPerLayer.size();
	this->layers.reserve(layerAmount);
	for (size_t layerSize : nodesPerLayer)
	{
		this->layers.emplace_back(Layer(layerSize));
	}

	// creates connections between the layers
	this->connectAllLayers();
}


void DNeuralNetwork::createNetworkStructure(
	const std::vector<std::pair<size_t, ActivationFunctionType>>&
	nodesAndFunctionPerlayer)
{
	// clears any existing layers before creating them
	this->clear();

	// creates layers with specified node amounts and functions
	size_t layerAmount = nodesAndFunctionPerlayer.size();
	this->layers.reserve(layerAmount);
	for (auto layerStats : nodesAndFunctionPerlayer)
	{
		this->layers.emplace_back(
			Layer(layerStats.first, layerStats.second));
	}

	// creates connections between the layers
	this->connectAllLayers();
}


size_t DNeuralNetwork::getLayerAmount()
{
	return this->layers.size();
}


size_t DNeuralNetwork::getNodeAmount()
{
	size_t amount = 0;
	for (Layer& layer : this->layers)
	{
		amount += layer.getNodeAmount();
	}
	return amount;
}


size_t DNeuralNetwork::getConnectionAmount()
{
	size_t amount = 0;
	for (Layer& layer : this->layers)
	{
		amount += layer.getConnectionsAmount();
	}
	return amount;
}


size_t DNeuralNetwork::getNodeAmountInLayer(size_t layerIndex)
{
	return this->layers[layerIndex].getNodeAmount();
}


size_t DNeuralNetwork::getConnectionAmountInLayer(size_t layerIndex)
{
	return this->layers[layerIndex].getConnectionsAmount();
}


double& DNeuralNetwork::getBiasInLayer(
	size_t layerIndex,
	size_t nodeIndex)
{
	return this->layers[layerIndex].getBiasByIndex(nodeIndex);
}


double& DNeuralNetwork::getWeightInLayer(size_t layerIndex, size_t weightIndex)
{
	return this->layers[layerIndex].getWeightByIndex(weightIndex);
}


void DNeuralNetwork::mutate(double mutationScale)
{
	// creates a 0.5 chance of coefficient having a different sign
	mutationScale *= (
		RandomGenerator::generateRandomInteger<int>(0, 1) ?
		(1.0) : (-1.0));

	// ========== a very bad mutation algorythm ==========

	// Gets amount of weights + nodes and selects a random element from them
	size_t elementAmount =
		this->getNodeAmount() + this->getConnectionAmount();
	size_t elementsToTraverse =
		RandomGenerator::generateRandomInteger<size_t>(
			this->getNodeAmountInLayer(0) + 1, elementAmount);

	// mutates selected element
	this->mutateElement(elementsToTraverse, mutationScale);
}


void DNeuralNetwork::mutateElement(
	size_t elementIndex,
	double mutationScale)
{
	// ========== a very bad mutation algorythm ==========

	// finds the element to mutate by substracting
	// elements amounts from element index,
	// until index lands in the current layer's biases or weights
	for (Layer& layer : this->layers)
	{
		// mutation order:
		// bias
		// weight
		if (elementIndex <= layer.getNodeAmount())
		{
			layer.mutateRandomBias(mutationScale);
		}
		else
		{
			elementIndex -= layer.getNodeAmount();
			if (elementIndex <= layer.getConnectionsAmount())
			{
				layer.mutateRandomWeight(mutationScale);
			}
			else
			{
				elementIndex -= layer.getConnectionsAmount();
			}
		}
	}
}


void DNeuralNetwork::randomize(bool randomizeWeights, bool randomizeBiases)
{
	for (Layer& layer : this->layers)
	{
		if (randomizeWeights)
		{
			layer.randomizeWeights();
		}

		// randomize biases if this is not the first layer
		if (randomizeBiases && (&layer != &this->layers.front()))
		{
			layer.randomizeBiases();
		}
	}
}


void DNeuralNetwork::clear()
{
	this->layers.clear();
	this->layers.shrink_to_fit();
}


void DNeuralNetwork::generateOutputs(const std::vector<double>& input)
{
	this->processInputLayer(input);
	this->processHiddenLayers();
	this->processLastLayer();
}


std::vector<double> DNeuralNetwork::getOutputs()
{
	// returns the outputs of the last layer
	return this->layers.back().getOutputVector();
}


std::vector<double> DNeuralNetwork::processValues(
	const std::vector<double>& input)
{
	this->generateOutputs(input);
	return this->getOutputs();
}


bool DNeuralNetwork::hasSameStructureAs(const std::vector<size_t>& nodesPerlayer)
{
	return (this->getNodesPerLayer() == nodesPerlayer);
}


bool DNeuralNetwork::hasSameStructureAs(DNeuralNetwork& otherNetwork)
{
	return this->hasSameStructureAs(otherNetwork.getNodesPerLayer());
}


bool DNeuralNetwork::operator==(const DNeuralNetwork& otherNetwork)
{
	// compares activation function and structure
	if (std::equal(
			this->layers.begin(),
			this->layers.end(),
			otherNetwork.layers.begin()))
	{
		return true;
	}

	return false;
}


bool DNeuralNetwork::operator!=(const DNeuralNetwork& otherNetwork)
{
	return !(*this == otherNetwork);
}


Layer& DNeuralNetwork::lastLayer()
{
	return this->layers.back();
}


Layer& DNeuralNetwork::firstLayer()
{
	return this->layers.front();
}


std::vector<size_t> DNeuralNetwork::getNodesPerLayer()
{
	std::vector<size_t> nodesPerLayer;
	nodesPerLayer.reserve(this->getLayerAmount());
	for (Layer& layer : this->layers)
	{
		nodesPerLayer.emplace_back(layer.getNodeAmount());
	}

	return nodesPerLayer;
}


std::vector<std::vector<double>> DNeuralNetwork::getWeightsPerLayer()
{
	std::vector<std::vector<double>> weightsPerLayer;

	size_t layersWithWeightsAmount = this->getLayerAmount() - 1;
	weightsPerLayer.reserve(layersWithWeightsAmount);

	// copis all weights until the last layer
	for (size_t i = 0; i < layersWithWeightsAmount; ++i)
	{
		weightsPerLayer.emplace_back(this->layers[i].getWeights());
	}

	return weightsPerLayer;
}


std::vector<std::vector<double>> DNeuralNetwork::getBiasesPerLayer()
{
	std::vector<std::vector<double>> biasesPerLayer;

	biasesPerLayer.reserve(this->getLayerAmount());
	for (Layer& layer : this->layers)
	{
		biasesPerLayer.emplace_back(layer.getBiases());
	}

	return biasesPerLayer;
}


std::vector<ActivationFunctionType> DNeuralNetwork::functionsPerLayer()
{
	std::vector<ActivationFunctionType> functionsPerLayer;

	functionsPerLayer.reserve(this->getLayerAmount());
	for (Layer& layer : this->layers)
	{
		functionsPerLayer.emplace_back(
			layer.getActivationFunction());
	}

	return functionsPerLayer;
}

void DNeuralNetwork::setActivationFunctionInLayer(
	size_t layerIndex,
	ActivationFunctionType type)
{
	this->layers[layerIndex].setActivationFunction(type);
}

ActivationFunctionType& DNeuralNetwork::getActivationFunctionInLayer(
	size_t layerIndex)
{
	return this->layers[layerIndex].getActivationFunction();
}


void DNeuralNetwork::copyWeightsFrom(DNeuralNetwork& copyFrom)
{
	this->copyWeightsFrom(copyFrom.getWeightsPerLayer());
}


void DNeuralNetwork::copyWeightsFrom(
	const std::vector<std::vector<double>>& copyFrom)
{
	// itterates through each layer until reached last layer
	size_t maxConnectedLayerIndex = this->getLayerAmount() - 2;
	for (size_t layerIndex = 0;
		layerIndex <= maxConnectedLayerIndex;
		++layerIndex)
	{
		this->layers[layerIndex].setWeights(copyFrom[layerIndex]);
	}
}


void DNeuralNetwork::copyBiasesFrom(DNeuralNetwork& network)
{
	this->copyBiasesFrom(network.getBiasesPerLayer());
}


void DNeuralNetwork::copyBiasesFrom(
	const std::vector<std::vector<double>>& biasesPerLayer)
{
	size_t layerAmount = this->getLayerAmount();
	for (size_t i = 0; i < layerAmount; ++i)
	{
		this->layers[i].setBiases(biasesPerLayer[i]);
	}
}


void DNeuralNetwork::connectAllLayers()
{
	// connects all layers to next ones, except for last (no connection needed)
	size_t layerMaxIndex = this->layers.size() - 1;
	for (size_t index = 0; index < layerMaxIndex; ++index)
	{
		this->layers[index].connectToLayer(&this->layers[index + 1]);
	}
}


void DNeuralNetwork::processInputLayer(const std::vector<double>& inputs)
{
	// forces the first layer to fire the input values
	this->layers.front().forceLayerFire(inputs, &this->layers[1]);
}


void DNeuralNetwork::processHiddenLayers()
{
	size_t hiddenLayerMaxIndex = this->layers.size() - 2;
	for (size_t i = 1; i <= hiddenLayerMaxIndex; ++i)
	{
		this->layers[i].processLayer(&this->layers[i + 1]);
	}
}


void DNeuralNetwork::processLastLayer()
{
	this->layers.back().calculateNodeOutputs();
}

} // namespace
