#pragma once

#include "Connection.h"
#include "ActivationFunctionType.h"
#include "DerivativeCalculator.h"

namespace dneural
{

// Node (aka Neuron) can calculate its output by putting the input sum
// through the activation function, and can send out the result to
// other nodes through a connection
class Node
{
public:
	Node();
	Node(double bias);

	// Calculates the output by putting input sum through activation function
	double& calculateOutput(ActivationFunctionType functionType);

	// Manualy sets the output, which is then ready to be sent
	void setOutput(double newOutput);

	// Sends node output to the target through the conenction
	void sendOutputTo(Node* target, Connection* connection);

	// Returns the calculated output.
	// This function does not re-calculate the output
	double& getOutput();

	void setBias(double newBias);
	void changeBias(double changeBy);
	double& getBias();

	void setInputSum(double newSum);
	void changeInputSum(double changeBy);
	double& getInputSum();

	// randomizes bias between -1 and 1
	void randomizeBias();

	// Returns the node bias when converted to double
	operator double();

	// Compares biases
	bool operator==(const Node& other);

private:
	// Returns the output of the chosen activation function
	// with the passed value as its argument
	double activationFunction(ActivationFunctionType type,
							  double value);

	double inputSum = 0.0;
	double bias = 0.0;
	double output = 0.0;
};

} // namespace
