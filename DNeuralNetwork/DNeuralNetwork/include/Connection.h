#pragma once

#include "RandomGenerator.h"

namespace dneural
{

// Keeps the weight of connection
class Connection
{
public:
	// Default constructor, generates random weight in range -1 to 1
	Connection();

	// This constructor sets the weight same value as the argument
	Connection(double presetWeight);

	double& getWeight();
	void setWeight(double newWeight);
	void changeWeight(double changeBy);

	// Returns the input * weight
	double generateOutput(double input);

	// Randomizes weight between -1 and 1
	void randomizeWeight();

	// Returns weight when converted to double
	operator double();

	// Compares weights
	bool operator==(const Connection& other);

private:
	double weight = 0.0;
};

} // namespace
