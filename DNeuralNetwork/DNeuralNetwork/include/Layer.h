#pragma once

#include <vector>

#include "Node.h"
#include "Connection.h"

namespace dneural
{

// Layer class serves as container and coordinator for nodes and
// outgoing connections
class Layer
{
public:
	// Creates specified node amount
	Layer(size_t nodeAmount);

	// Creates specified node amount and sets activation function
	Layer(size_t nodeAmount, ActivationFunctionType functionType);

	// Creates connections between this and a specified layer
	void connectToLayer(Layer* connectTo);

	size_t getNodeAmount();
	size_t getConnectionsAmount();

	double& getWeightByIndex(size_t index);
	double& getBiasByIndex(size_t index);

	void setWeights(const std::vector<double>& weights);
	void setBiases(const std::vector<double>& biases);

	std::vector<double> getWeights();
	std::vector<double> getBiases();

	// Returns std::vector of node output values
	std::vector<double> getOutputVector();

	// Returns the output value of the node with the specified index
	double getOutputByNodeIndex(size_t index);

	void mutateRandomWeight(double mutationScale);
	void mutateRandomBias(double mutationScale);

	void randomizeWeights();
	void randomizeBiases();

	// Sets node outputs with values in a vector and fires them
	void forceLayerFire(
		const std::vector<double>& outputs,
		Layer* nextLayer);

	// Processes this layer's nodes and sends the weighted output
	// to the next layer's nodes.
	// Internally calls calculateNodeOutputs() and fireNodes()
	void processLayer(Layer* nextLayer);

	// makes nodes calculate their output based on the input sum
	void calculateNodeOutputs();

	// Sets activation function for nodes in the layer
	void setActivationFunction(ActivationFunctionType type);
	ActivationFunctionType& getActivationFunction();

	// Sends node outputs to the next layer, altering them
	// with connection weights. Does not recalculate ndoe outputs
	void fireNodes(Layer* nextLayer);

	// Compares the structure, activation function, biases and weights
	bool operator==(const Layer& otherLayer);

	// These store nodes and outgoign connections
	std::vector<Node> nodes;
	std::vector<Connection> connections;

private:
	void createNodes(size_t nodeAmount);
	void createConnections(size_t connectionAmount);

	ActivationFunctionType activationFunctionType =
		ActivationFunctionType::sigmoid;
};

} // namespace
