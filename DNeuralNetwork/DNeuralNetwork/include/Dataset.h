#pragma once

#include <vector>

namespace dneural
{

// Contains an amount of input-output pairs
class Dataset
{
public:

	// Stores input-output pair
	class Example
	{
	public:
		Example(
			const std::pair<
				std::vector<double>,
				std::vector<double>>& inputOutputPair);

		std::vector<double> inputs;
		std::vector<double> outputs;

		Example& operator=(
			const std::pair<
				std::vector<double>,
				std::vector<double>>& inputOutputPair);
	};

	std::vector<Example> examples;

	// Returns training example reference of specified index
	const Example& operator[](size_t exampleIndex) const;

	// Initializes all training examples
	Dataset(
		const std::vector<
			std::pair<
				std::vector<double>,
				std::vector<double>>>& trainingExampels);

	Dataset() {}

	// Assigns a dataset and returns reference to oneself
	Dataset& operator=(
		const std::vector<
			std::pair<
				std::vector<double>,
				std::vector<double>>>& trainingExampels);

	// Returns amount of examples
	size_t size() const;
};

} // namespace
