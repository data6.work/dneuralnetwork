#pragma once

#include <vector>

#include "DNeuralNetwork.h"
#include "NetworkTeacher.h"
#include "Dataset.h"

namespace dneural
{

// Provides a simple interface for the network
class SimpleNetwork
{
public:
	// Creates a network with specified node amount per layers
	SimpleNetwork(const std::vector<size_t>& nodesPerLayer);

	// Processes values and returns the results
	std::vector<double> process(const std::vector<double>& inputs);

	// Teaches network to process dataset inputs better
	void teach(const Dataset& dataset, double learningRate = 0.15);

	// Mutates a random network element.
	// Mutation scale should be a small value (0.1 at max)
	void mutate(double mutationScale = 0.01);

	// Copies the network
	void operator=(const SimpleNetwork& other);
	void operator=(const DNeuralNetwork& other);

	// Returns the pointer to the member network
	operator DNeuralNetwork*();
	operator DNeuralNetwork&();

private:
	DNeuralNetwork network;
};

} // naemspace
