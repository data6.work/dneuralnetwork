#pragma once

#include <cmath>

#include "ActivationFunctionType.h"

namespace dneural
{
namespace DerivativeCalculator
{

// Calculates derivatives of the activation function type
double calculateDerivative(
	double value, ActivationFunctionType function);


}
} // namespaces
