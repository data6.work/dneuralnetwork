#include <iostream>

#include "DNeuralNetwork.h"
#include "NetworkTeacher.h"

void printInputsAndOututs(
	const std::vector<double>& inputs,
	const std::vector<double>& outputs);

void runXOR(dneural::DNeuralNetwork* net);

int main()
{
	dneural::DNeuralNetwork net({ 2, 3, 1 });

	// prints the current network output on XOR problem
	runXOR(&net);

	std::cout << "\nItterating until learning loss reaches 0.001...\n\n";

	// initializing the training dataset for XOR problem
	dneural::Dataset dataset({
		{{0., 0.}, {0.}},
		{{0., 1.}, {1.}},
		{{1., 0.}, {1.}},
		{{1., 1.}, {0.}}
	});
	
	// adapts the network for the dataset
	dneural::NetworkTeacher::teachForItterations(&net, dataset, 1000, 0.5);

	// prints network outputs after the learning
	runXOR(&net);

	while (true);

	return 0;
}


void printInputsAndOututs(
	const std::vector<double>& inputs,
	const std::vector<double>& outputs)
{
	std::cout << "Net inputs: ";
	for (double value : inputs)
	{
		std::cout << value << ", ";
	}
	std::cout << std::endl;

	std::cout << "Net outputs: ";
	for (double value : outputs)
	{
		std::cout << value << ", ";
	}
	std::cout << std::endl;
}


void runXOR(dneural::DNeuralNetwork* net)
{
	std::vector<double> inputs;
	std::vector<double> outputs;

	inputs = { 0, 0 };
	printInputsAndOututs(inputs, net->processValues(inputs));

	inputs = { 0, 1 };
	printInputsAndOututs(inputs, net->processValues(inputs));

	inputs = { 1, 0 };
	printInputsAndOututs(inputs, net->processValues(inputs));

	inputs = { 1, 1 };
	printInputsAndOututs(inputs, net->processValues(inputs));
}
