#include "pch.h"
#include "DNeuralNetwork.h"
#include "NetworkFileIO.h"
using namespace dneural;

namespace
{

TEST(NetworkFileIO, PropperLoadingTest)
{
	dneural::DNeuralNetwork source({
		{5, ActivationFunctionType::hyperbolicTangent},
		{3, ActivationFunctionType::hyperbolicTangent},
		{2, ActivationFunctionType::sigmoid}});

	source.randomize(true, true);

	dneural::DNeuralNetwork loadTo;
	dneural::DNeuralNetwork returned;

	const std::string filename = "owo.txt"; // yes

	for (int i = 0; i < 4; ++i)
	{
		EXPECT_NO_FATAL_FAILURE(NetworkFileIO::save(&source, filename));
		EXPECT_NO_FATAL_FAILURE(NetworkFileIO::load(&loadTo, filename));
		EXPECT_NO_FATAL_FAILURE(returned = NetworkFileIO::load(filename));

		bool loadingToNetworkSuccess = loadTo == source;
		bool returningNetworkSuccess = returned == source;

		EXPECT_TRUE(loadingToNetworkSuccess && returningNetworkSuccess);
	}
}

}
