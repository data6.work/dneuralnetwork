#include "pch.h"
#include "DNeuralNetwork.h"
#include "NetworkTeacher.h"
using namespace dneural;

namespace
{

template <typename T>
std::vector<T> vectorSubstract(std::vector<T> a, std::vector<T> b)
{
	size_t size = a.size();

	std::vector<T> toReturn;
	toReturn.resize(size);

	for (unsigned i = 0; i < size; ++i)
	{
		toReturn[i] = a[i] - b[i];
	}

	return toReturn;
}

template <typename T>
double vectorDistance(std::vector<T> a, std::vector<T> b)
{
	double sum = 0.0;
	for (unsigned i = 0; i < a.size(); ++i)
	{
		sum += pow(a[i] - b[i], 2.0);
	}

	return sqrt(sum);
}

template <typename T>
double meanSquareError(std::vector<T> a, std::vector<T> b)
{
	double sum = 0.0;
	for (unsigned i = 0; i < a.size(); ++i)
	{
		sum += pow(a[i] - b[i], 2.0);
	}

	return sum / double(a.size());
}


TEST(Backpropagation, XORTraining)
{
	dneural::DNeuralNetwork net({ 2, 3, 1 });

	// initializing the training dataset for XOR problem
	dneural::Dataset dataset(
		{
			{{0., 0.}, {0.}},
			{{0., 1.}, {1.}},
			{{1., 0.}, {1.}},
			{{1., 1.}, {0.}}
		});

	// adapts the network for the dataset
	EXPECT_NO_FATAL_FAILURE(
		dneural::NetworkTeacher::teachUnilLearningLoss(
			&net,
			dataset,
			0.01,
			0.5));
}

} // namespace
