#include "pch.h"
#include "Connection.h"

namespace
{

TEST(Connection, Construction)
{
	EXPECT_NO_FATAL_FAILURE(dneural::Connection());
}


TEST(Connection, OutputGeneration)
{
	const double weight = 5.0;
	const double input = -2;
	const double expected = weight * input;

	dneural::Connection connection;
	connection.setWeight(5.0);
	ASSERT_EQ(connection.generateOutput(input), expected);
}

} // namespace
