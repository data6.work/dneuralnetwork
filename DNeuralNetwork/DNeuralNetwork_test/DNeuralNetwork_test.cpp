#include "pch.h"
#include "DNeuralNetwork.h"
using namespace dneural;

namespace
{

TEST(DNeuralNetwork, Construction)
{
	EXPECT_NO_FATAL_FAILURE(dneural::DNeuralNetwork net);
	EXPECT_NO_FATAL_FAILURE(dneural::DNeuralNetwork net({
		{2, ActivationFunctionType::sigmoid},
		{2, ActivationFunctionType::hyperbolicTangent}
		}));
}



TEST(DNeuralNetwork, Processing)
{
	const std::vector<size_t> nodesPerLayers = { 1, 1 };
	const double weight = -2.1;
	const std::vector<double> input = { 0.42 };

	dneural::DNeuralNetwork network;
	network.copyWeightsFrom({ {weight} });

	network.generateOutputs(input);

	double expected = 1.0 / (1.0 + exp(-(input[0] * weight)));
	std::vector<double> output = network.getOutputs();

	// compares the network output with the expected output
	EXPECT_EQ(output[0], expected);
}


TEST(DNeuralNetwork, NoCrashOnCustomStructure)
{
	std::vector<size_t> nodesInLayers = { 32, 10, 2 };

	EXPECT_NO_FATAL_FAILURE(
		dneural::DNeuralNetwork net(nodesInLayers));
}


TEST(DNeuralNetwork, NoCrashOnMutations)
{
	DNeuralNetwork net({ 5, 6, 3 });

	for (int i = 0; i < 1000; ++i)
	{
		EXPECT_NO_FATAL_FAILURE(net.mutate(0.01));
	}
}


TEST(DNeuralNetwork, NetworkComparison)
{
	DNeuralNetwork net1({ 10, 1, 2 });
	DNeuralNetwork net2({ 10, 5, 2 });
	EXPECT_TRUE(net1 != net2);

	DNeuralNetwork net3 = net1;
	net3.setActivationFunctionInLayer(1, ActivationFunctionType::hyperbolicTangent);
	net1.setActivationFunctionInLayer(1, ActivationFunctionType::hyperbolicTangent);
	EXPECT_TRUE(net1 == net3);

	net1.setActivationFunctionInLayer(0, ActivationFunctionType::hyperbolicTangent);
	EXPECT_TRUE(net1 != net3);
}


TEST(DNeuralNetwork, NetworkCopying)
{
	DNeuralNetwork net1({ 10, 1, 2 });
	DNeuralNetwork net2({ 10, 5, 2 });

	EXPECT_TRUE(net1 != net2);
	net2 = net1;
	EXPECT_TRUE(net1 == net2);

	net2.mutate(0.1);
	EXPECT_TRUE(net1 != net2);
}

} // namespace
