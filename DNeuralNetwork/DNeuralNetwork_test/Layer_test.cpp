#include "pch.h"
#include "Layer.h"
using namespace dneural;

namespace
{

TEST(Layer, Construction)
{
	dneural::Layer* layerPtr = nullptr;
	EXPECT_NO_FATAL_FAILURE(layerPtr = new dneural::Layer(42));

	EXPECT_EQ(layerPtr->getNodeAmount(), 42);

	delete layerPtr;
}


TEST(Layer, NoCrashOnLayerConnecting)
{
	dneural::Layer l0(2);
	dneural::Layer l1(4);
	dneural::Layer l2(2);

	EXPECT_NO_FATAL_FAILURE(l0.connectToLayer(&l1));
	EXPECT_NO_FATAL_FAILURE(l1.connectToLayer(&l2));
}


TEST(Layer, NoCrashOnLayerProcessing)
{
	ActivationFunctionType ftype = ActivationFunctionType::sigmoid;

	dneural::Layer inputL(3);
	dneural::Layer hiddenL(4, ActivationFunctionType::hyperbolicTangent);
	dneural::Layer outputL(3);

	inputL.connectToLayer(&hiddenL);
	hiddenL.connectToLayer(&outputL);

	std::vector<double> inputVals = { 0.6, -0.12, 0.24 };

	EXPECT_NO_FATAL_FAILURE(
		inputL.forceLayerFire(inputVals, &hiddenL));

	EXPECT_NO_FATAL_FAILURE(hiddenL.processLayer(&outputL));

	EXPECT_NO_FATAL_FAILURE(outputL.calculateNodeOutputs());
}

} // namespace
