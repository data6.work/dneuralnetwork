#include "pch.h"

namespace
{

// Tests if the GTest framework is working correctly
TEST(_GTestSanityTest, _EQTest)
{
	EXPECT_EQ(1, 1);
	EXPECT_NE(1, 0);
}

}
