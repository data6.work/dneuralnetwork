#include "pch.h"
#include "Node.h"
#include "Connection.h"
#include "ActivationFunctionType.h"
using namespace dneural;

namespace
{

TEST(Node, Construction)
{
	EXPECT_NO_FATAL_FAILURE(dneural::Node());
}


TEST(Node, ValueManipulations)
{
	const double bias = 0.5;
	const double inputSum = 1.6;
	const double changeBy = -0.1;

	dneural::Node node;
	node.setBias(bias);
	node.setInputSum(inputSum);

	node.changeBias(changeBy);
	node.changeInputSum(changeBy);

	EXPECT_TRUE(node.getBias() == bias + changeBy);
	EXPECT_TRUE(node.getInputSum() == inputSum + changeBy);
}


TEST(Node, OutputGeneration)
{
	ActivationFunctionType function = 
		ActivationFunctionType::hyperbolicTangent;
	const double bias = 0.5;
	const double inputSum = 1.6;
	const double changeBy = -0.1;
	const double expected = tanh(inputSum + bias);

	dneural::Node node;
	node.setBias(bias);
	node.setInputSum(inputSum);

	double nodeOutput = node.calculateOutput(function);

	EXPECT_TRUE(nodeOutput == expected);
}

} // namespace