In this project i'm creating a neural network class library, which would
allow for an easy creation of feed-forward neural networks in C++ with
any configuration and different activation functions per layer.

The main goals of the network library is to allow for ceation of a simple fully
initialized neural network with as little as 1 line of code.

For the sake of flexibility and memory usage being proportional to the network
elements amount, this network will only feature feedforward networks

C++ version used in the project is C++17 to support the std::pair
