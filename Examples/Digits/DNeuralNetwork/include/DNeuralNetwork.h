#pragma once

#include <vector>

#include "Layer.h"
#include "Node.h"
#include "Connection.h"

namespace dneural
{

// Network class contains and manages the neural network.
class DNeuralNetwork
{
public:
	// Creates a network with the specified layer
	// structure and random weights in range [-1; 1].
	DNeuralNetwork(const std::vector<size_t>& nodesPerLayer = { 1, 1 });

	// Creates a network with the specified layer
	// structure and random weights in range [-1; 1] and specified
	// activation function in each layer
	DNeuralNetwork(
		const std::vector<std::pair<size_t, ActivationFunctionType>>& 
		nodesAndFunctionPerLayer);

	// Creates network structure with the specified nodes per layer amount.
	void createNetworkStructure(const std::vector<size_t>& nodesPerLayer);

	// Creates network structure with the specified nodes per layer amount
	// and activation functions per layer
	void createNetworkStructure(
		const std::vector<std::pair<size_t, ActivationFunctionType>>&
		nodesAndFunctionPerLayer);

	void copyWeightsFrom(DNeuralNetwork& network);
	void copyWeightsFrom(
		const std::vector<std::vector<double>>& weightsPerLayer);

	void copyBiasesFrom(DNeuralNetwork& network);
	void copyBiasesFrom(
		const std::vector<std::vector<double>>& biasesPerLayer);

	std::vector<size_t> getNodesPerLayer();
	std::vector<std::vector<double>> getWeightsPerLayer();
	std::vector<std::vector<double>> getBiasesPerLayer();
	std::vector<ActivationFunctionType> functionsPerLayer();

	void setActivationFunctionInLayer(
		size_t layerIndex,
		ActivationFunctionType type);
	ActivationFunctionType& getActivationFunctionInLayer(
		size_t layerIndex);

	size_t getLayerAmount();
	size_t getNodeAmount();
	size_t getConnectionAmount();

	size_t getNodeAmountInLayer(size_t layerIndex);
	size_t getConnectionAmountInLayer(size_t layerIndex);

	double& getBiasInLayer(size_t layerIndex, size_t nodeIndex);
	double& getWeightInLayer(size_t layerIndex, size_t weightIndex);

	// Wipes all layers in the network.
	void clear();

	// Processes the input values.
	// Call getOutputs() to recieve the result
	void processValues(const std::vector<double>& input);

	// Returns the network outputs
	std::vector<double> getOutputs();

	// Mutates random network weight or bias by the specified amount.
	// There is a 1:1 chance that scale will change its sign.
	void mutate(double mutationScale);

	// Randomizes weights and/or biases
	void randomize(bool randomizeWeights, bool randomizeBiases);

	// Returns true if this network's node amount per layer matches
	// passed node amount per layer
	bool hasSameStructureAs(const std::vector<size_t>& nodesPerlayer);
	bool hasSameStructureAs(DNeuralNetwork& otherNetwork);

	// Returns true if networks are identical in structure, activation
	// function and weights and bias values.
	// Temporary values such as output values and errors are not compared
	bool operator==(const DNeuralNetwork& otherNetwork);
	bool operator!=(const DNeuralNetwork& otherNetwork);

	Layer& lastLayer();
	Layer& firstLayer();

	// Stores all layers
	std::vector<Layer> layers;

private:
	// Connects all layers to each other
	void connectAllLayers();

	// Mutates passed element
	void mutateElement(size_t elementIndex, double mutationScale);

	void processInputLayer(const std::vector<double>& inputs);
	void processHiddenLayers();
	void processLastLayer();
};

} // namespace 
