#pragma once
#include <random>
#include <ctime>

namespace dneural
{

// Custom class for generating random values, because f**k rand()
class RandomGenerator
{
public:
	static double generateRandomDouble(double from, double to)
	{
		return std::uniform_real_distribution<double>(from, to)(
			RandomGenerator::randomEngine);
	}

	template<typename Type = int>
	static Type generateRandomInteger(Type from, Type to)
	{
		return std::uniform_int_distribution<Type>(from, to)(
			RandomGenerator::randomEngine);
	}

private:
	static std::default_random_engine randomEngine;
};

} // namespace
