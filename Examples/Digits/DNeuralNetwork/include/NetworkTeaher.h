#pragma once

#include "DNeuralNetwork.h"
#include "Dataset.h"

namespace dneural
{

// Teaches network through backpropagation to reduce
// the overall learning cost on the dataset
class NetworkTeacher
{
public:
	// Teaches network until learning loss is less than specified.
	// Dont put learning loss too low
	static void teachUnilLearningLoss(
		DNeuralNetwork* network,
		const Dataset& dataset,
		double lossRequired,
		double learningRate);

	// Teaches the network on the dataset for specified
	// amount of itterations
	static void teachForItterations(
		DNeuralNetwork* network,
		const Dataset& dataset,
		size_t itterationsAmount,
		double learningRate);

private:
	// Itterates through all dataset values and adapts network
	// to them
	static void trainOnDataset(
		DNeuralNetwork* network,
		const Dataset& dataset,
		double learningRate);

	// Alters passed weights and biases vectors to reduce
	// the error
	static void backpropagate(
		DNeuralNetwork* network,
		std::vector<std::vector<double>>& newWeights,
		std::vector<std::vector<double>>& newBiases,
		std::vector<std::vector<double>>& errors);

	// Returns vector storing vectors with given sizes
	static std::vector<std::vector<double>> create2DVectorArray(
		std::vector<size_t> vectorSizes);

	static void nullify2DVector(std::vector<std::vector<double>>* vector);

	// Averages the cost over all dataset
	static double getLossOnDataset(
		DNeuralNetwork* network,
		const Dataset& dataset);
};

} // namespace
