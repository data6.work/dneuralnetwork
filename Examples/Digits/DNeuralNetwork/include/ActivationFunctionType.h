#pragma once

namespace dneural
{

// Names for activation functions
enum ActivationFunctionType
{
	sigmoid,

	hyperbolicTangent,

	ReLU,

	// Identical to ReLU, but has 0.01 rate of change on x < 0
	LReLU,

	identity,

	cube
};

} // namespace
