#pragma once

#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <iomanip>

#include "DNeuralNetwork.h"

namespace dneural
{

// Class for easy saving of the network network to a file.
// File is stored in a binary format.
class NetworkFileIO
{
public:
	// Writes the network data to a file
	static void save(
		DNeuralNetwork* network,
		const std::string& filename);

	// Loads the network data from a file to the passed network
	static void load(
		DNeuralNetwork* network,
		const std::string& filename);

	// Returns the network loaded from file
	static DNeuralNetwork load(
		const std::string& filename);

private:
	// ========== SAVING ==========

	// Coordinates the saving of all network
	static void saveParameters(
		DNeuralNetwork* network,
		std::ofstream* file);

	// Saves parameters related to structure such as nodes per layer
	// and activation functions types per layer
	static void saveStructuralParameters(
		DNeuralNetwork* network,
		std::ofstream* file);

	// Saves network such as weights and biases
	static void saveValueParameters(
		DNeuralNetwork* network,
		std::ofstream* file);

	// Function used for writing to files
	template<typename T>
	static void writeValue(const T& value, std::ofstream* file)
	{
		if (!file->write((char*)(&value), sizeof(T)))
		{
			// throw on fail
			throw -3;
		}
	}

	// ========== LOADING ==========

	// Coordinates the loading of all network
	static void loadParameters(
		DNeuralNetwork* network,
		std::ifstream* file);

	static void loadStructuralParameters(
		DNeuralNetwork* network,
		std::ifstream* file);

	static void loadValueParameters(
		DNeuralNetwork* network,
		std::ifstream* file);

	// Function used for reading from files
	template<typename T>
	static void readValue(
		T* valueToUpdate,
		std::ifstream* file,
		size_t size = sizeof(T))
	{
		if (!file->read((char*)valueToUpdate, size))
		{
			// throw on fail
			throw -3;
		}
	}
};

} // namespace
