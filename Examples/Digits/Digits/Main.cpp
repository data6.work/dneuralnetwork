#include <iostream>
#include <thread>

#include "DNeuralNetwork.h"
#include "NetworkTeaher.h"

#include "SegmentDisplay.h"
#include "InputProcessor.h"

// generates a vector of 0's except for 1 at specified position
std::vector<double> generateVectorWith1At(size_t at);

// prits controls
void printInstructions();

int main()
{
	printInstructions();

	sf::RenderWindow window(
		{ 200, 350 },
		"Digits",
		sf::Style::Titlebar | sf::Style::Close);

	// creates a 7 segment display
	SegmentDisplay segmentDisplay;

	// creates a network with 7 inputs for segment statuses,
	// 10 outputs for guesses on the number from 0 to 9
	dneural::DNeuralNetwork network({ 7, 10 });

	// creates a dataset, where inputs are segment statuses for
	// digits from 0 to 9, and outputs are 10 values representing
	// which number is it
	dneural::Dataset dataset({
		{{1, 1, 1, 0, 1, 1, 1}, generateVectorWith1At(0)},
		{{0, 0, 1, 0, 0, 1, 0}, generateVectorWith1At(1)},
		{{1, 0, 1, 1, 1, 0, 1}, generateVectorWith1At(2)},
		{{1, 0, 1, 1, 0, 1, 1}, generateVectorWith1At(3)},
		{{0, 1, 1, 1, 0, 1, 0}, generateVectorWith1At(4)},
		{{1, 1, 0, 1, 0, 1, 1}, generateVectorWith1At(5)},
		{{1, 1, 0, 1, 1, 1, 1}, generateVectorWith1At(6)},
		{{1, 0, 1, 0, 0, 1, 0}, generateVectorWith1At(7)},
		{{1, 1, 1, 1, 1, 1, 1}, generateVectorWith1At(8)},
		{{1, 1, 1, 1, 0, 1, 1}, generateVectorWith1At(9)}
		});

	InputProcessor inputProcessor(
		&window, &network, &dataset, &segmentDisplay);

	while (window.isOpen())
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(10));

		inputProcessor.processInputs();

		window.clear();
		segmentDisplay.drawSegments(&window);
		window.display();
	}

	return 0;
}


std::vector<double> generateVectorWith1At(size_t at)
{
	std::vector<double> vec(10, 0.0);
	vec[at] = 1.0;
	return vec;
}


void printInstructions()
{
	std::cout << "Press space to guess the number on the display\n";
	std::cout << "Press enter to teach the network\n";
	std::cout << "Press left mouse button on display segment to toggle it\n";
}
