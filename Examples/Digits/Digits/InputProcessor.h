#pragma once

#include <iostream>

#include "SFML/Graphics.hpp"

#include "DNeuralNetwork.h"
#include "NetworkTeaher.h"
#include "SegmentDisplay.h"

// Reads and processes inputs, including network training
// and network guessing
class InputProcessor
{
public:
	InputProcessor(
		sf::RenderWindow* window,
		dneural::DNeuralNetwork* network,
		dneural::Dataset* dataset,
		SegmentDisplay* segmentDisplay);

	void processInputs();

private:
	void processMousePress();
	void processKeyPress();

	// Teaches network for 200 itterations
	void teachNetwork();

	// displays the guesses network made on which digits
	// are most likely drawn on the display
	void displayNetworkGuess();

	// storesobjects to operate on
	sf::RenderWindow* window = nullptr;
	dneural::DNeuralNetwork* network = nullptr;
	dneural::Dataset* dataset = nullptr;
	SegmentDisplay* segmentDisplay = nullptr;
};

