#include "Segment.h"



Segment::Segment()
{
	this->body.setFillColor(this->disabledColor);
}


Segment::~Segment()
{
}


void Segment::toggle()
{
	this->enabled = !this->enabled;

	// sets appropriate colors
	if (this->enabled)
	{
		this->body.setFillColor(this->enabledColor);
	}
	else
	{
		this->body.setFillColor(this->disabledColor);
	}
}
