#pragma once

#include "SFML/Graphics.hpp"

class Segment
{
public:
	Segment();
	~Segment();

	void toggle();

	bool enabled = false;
	sf::RectangleShape body;

private:
	const sf::Color disabledColor = sf::Color(50, 50, 50);
	const sf::Color enabledColor = sf::Color(255, 255, 0);
};

