#pragma once

#include <vector>

#include "Segment.h"

// Stores all segments and returns their stats
class SegmentDisplay
{
public:
	SegmentDisplay();
	~SegmentDisplay();

	void drawSegments(sf::RenderWindow* window);

	// Toggles segment status on pressed position
	void toggleSegmentOnPosition(sf::Vector2f position);

	// Returns a vector of segments statuses (0 or 1)
	std::vector<double> getSegmentStatuses();

	Segment segments[7];

	const float height = 350.f;
	const float width = 200.f;

private:
	void initSegments();

	// Returns segment index on position, nullptr if there is nothing
	Segment* getSegmentOnPosition(sf::Vector2f position);
};

