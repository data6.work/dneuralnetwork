#include "InputProcessor.h"



InputProcessor::InputProcessor(
	sf::RenderWindow* window,
	dneural::DNeuralNetwork* network,
	dneural::Dataset* dataset,
	SegmentDisplay* segmentDisplay) :
	window(window),
	network(network),
	dataset(dataset),
	segmentDisplay(segmentDisplay)
{
}


void InputProcessor::processInputs()
{
	sf::Event event;
	while (this->window->pollEvent(event))
	{
		switch (event.type)
		{
		case sf::Event::Closed:
			this->window->close();
			break;

		case sf::Event::MouseButtonPressed:
			this->processMousePress();
			break;

		case sf::Event::KeyPressed:
			this->processKeyPress();
			break;
		}
	}
}


void InputProcessor::processMousePress()
{
	// signals segment display to toggle displays
	this->segmentDisplay->toggleSegmentOnPosition(
		(sf::Vector2f)sf::Mouse::getPosition(*this->window));
}


void InputProcessor::processKeyPress()
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Enter))
	{
		this->teachNetwork();
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Space))
	{
		// displays netowr's opinion on what is on the display
		this->displayNetworkGuess();
	}
}


void InputProcessor::teachNetwork()
{
	size_t itterations = 500;
	double learningRate = 0.8;

	std::cout << "Teaching network for " << itterations <<" itterations and ";
	std::cout << "learning rate of " << learningRate << "... ";

	// teaches the network for 1000 itterations and 0.8 learning rate
	dneural::NetworkTeacher::teachForItterations(
		this->network, *this->dataset, itterations, learningRate);

	std::cout << "Done\n";
}


void InputProcessor::displayNetworkGuess()
{
	std::cout << "Network guesses:\n";

	// calculates network outputs
	this->network->processValues(
		this->segmentDisplay->getSegmentStatuses());
	std::vector<double> networkOutputs = this->network->getOutputs();

	// displays network outputs
	for (size_t number = 0; number < 10; ++number)
	{
		std::cout << "Number " << number << " guess: ";
		std::cout << networkOutputs[number] << std::endl;
	}

	// finds the most certain guess
	double bestGuess = 0;
	size_t bestGuessNumber = 0;
	for (size_t number = 0; number < 10; ++number)
	{
		if (networkOutputs[number] > bestGuess)
		{
			bestGuessNumber = number;
			bestGuess = networkOutputs[number];
		}
	}
	std::cout << "\nBest guess: " << bestGuessNumber << std::endl;
	std::cout << std::endl << std::endl;
}
