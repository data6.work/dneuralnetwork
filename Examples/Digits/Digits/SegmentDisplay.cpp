#include "SegmentDisplay.h"



SegmentDisplay::SegmentDisplay()
{
	this->initSegments();
}


SegmentDisplay::~SegmentDisplay()
{
}


void SegmentDisplay::drawSegments(sf::RenderWindow* window)
{
	for (size_t i = 0; i < 7; ++i)
	{
		window->draw(this->segments[i].body);
	}
}


void SegmentDisplay::toggleSegmentOnPosition(sf::Vector2f position)
{
	Segment* segment = this->getSegmentOnPosition(position);

	// toggle segment if there is one at position
	if (segment)
	{
		segment->toggle();
	}
}


std::vector<double> SegmentDisplay::getSegmentStatuses()
{
	std::vector<double> statuses;
	statuses.resize(7);

	for (size_t i = 0; i < 7; ++i)
	{
		// sets 1 if segment is enabled, 0 if disabled
		statuses[i] = (this->segments[i].enabled) ? (1.0) : (0.0);
	}

	return statuses;
}


void SegmentDisplay::initSegments()
{
	// segments numbering scheme:
	//     0
	//   1   2
	//     3
	//   4	 5
	//     6

	const sf::Vector2f verticalSize {50, 100};
	const sf::Vector2f horizontalSize { 100, 50 };

	this->segments[0].body.setSize(horizontalSize);
	this->segments[0].body.setPosition({ 50, 0 });

	this->segments[1].body.setSize(verticalSize);
	this->segments[1].body.setPosition({ 0, 50 });

	this->segments[2].body.setSize(verticalSize);
	this->segments[2].body.setPosition({ 150, 50 });

	this->segments[3].body.setSize(horizontalSize);
	this->segments[3].body.setPosition({ 50, 150 });

	this->segments[4].body.setSize(verticalSize);
	this->segments[4].body.setPosition({ 0, 200 });

	this->segments[5].body.setSize(verticalSize);
	this->segments[5].body.setPosition({ 150, 200 });

	this->segments[6].body.setSize(horizontalSize);
	this->segments[6].body.setPosition({ 50, 300 });
}


Segment* SegmentDisplay::getSegmentOnPosition(sf::Vector2f position)
{
	for (size_t i = 0; i < 7; ++i)
	{
		// returns segment index if it contains the position
		if (this->segments[i].body.getGlobalBounds().contains(position))
		{
			return &this->segments[i];
		}
	}

	return nullptr;;
}
